This project was developed as a personal project for further understanding of the topic "semantic analysis using coocurrencies" and getting some experience in the field of visualization. 

Idea: to visualize the semantic differences among parties in the german Bundestag. 

Data:  oficial minutes of the last 8 Bundestags sessions (as to 10/2019)

Output:

![Klima](/output/Klima.png)



Used technologies: NLTK, Stanford Tagger, Numpy, Networkx, Matplotlib