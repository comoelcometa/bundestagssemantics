import json
from collections import defaultdict
import nltk
import numpy as np
from numpy import linalg as la
from scipy import spatial
import matplotlib.pyplot as plt
import networkx as nx
from networkx import nx_agraph

test = [['leipzig',  'ist', 'eine',  'schöne',  'stadt'], ['leipzig', 'ist', 'eine', 'gruene', 'stadt'], ['die', 'stadt',  'ist', 'groß'], ['ich', 'mag', 'leipzig'], ['berlin', 'ist', 'eine', 'stadt'], ['ich', 'mag', 'berlin']]
path = 'data.json'
parteien = ['fdp', 'union', 'gruene', 'linke', 'afd', 'spd']
farben = [plt.cm.YlOrBr, plt.cm.Greys, plt.cm.Greens, plt.cm.RdPu, plt.cm.PuBu, plt.cm.Reds]

with open(path, 'r') as file:
    data = json.load(file)

#computes a dictionary of all types with their frequency in corpus
def getDict(sents):
    dict = defaultdict(lambda: 0)
    for sent in sents:
        for word in sent:
            dict[word] += 1

    return dict

#computes a coocurrence matrix + an index of the words (in the matrix)
def getCoocurrences(sents):
    dict = getDict(sents)
    pairs = list(dict.items())
    pairs.sort(key= lambda x: x[1], reverse=True)
    index = [x[0] for x in pairs]
    # print(index)
    coocurrences = np.zeros((len(index), len(index)), dtype=int)

    for sent in sents:
        for word_m in sent:
            if word_m.isalpha():
                m = index.index(word_m)
                for word_n in sent[sent.index(word_m)::]:
                    if word_n.isalpha() and index.index(word_n) is not m:
                        n = index.index(word_n)
                        coocurrences[m][n] += 1
                        coocurrences[n][m] += 1
    return dict, index, coocurrences


# def getDistances(matrix):
#     distances = np.zeros((len(matrix), len(matrix)))
#     # print(matrix.shape)
#     for m in range(matrix.shape[0]):
#         row = matrix[m]
#         for n in range(m + 1, matrix.shape[0]):
#             col = matrix[n]
#             dist = 1 - spatial.distance.cosine(row, col)
#             distances[n][m] = dist
#             distances[m][n] = dist
#     return distances

def process_single_word(word, words_num, matrix, index, mass):
    # dict, index, matrix = getCoocurrences(corpus)

    #gets vector of the chosen word
    term = matrix[index.index(word)]

    #computes distances to all other words
    term_distances = [mass(term, col) for col in matrix[index.index(word)]]
    term_distances = np.array(term_distances)
    term_distances[np.isnan(term_distances)] = 0
    # print([1-item for item in term_distances])
    # print(term_distances)
    # print(index)

    #computes n + 1 most similar words (most similar qword is word itself)
    most_similars_indexes = list(term_distances.argsort()[-words_num :])
    # print('indices:')
    # print(most_similars_indexes)
    # print('distances:')
    # print([term_distances[item] for item in most_similars_indexes])


    #computes a matrix with similarities among n most similar words (excluding word itself)
    distance_matrix = np.zeros((words_num, words_num))
    for m in range(words_num):
        distance_matrix[m][m] = 1
        word_m = most_similars_indexes[m]
        for n in range(m + 1, words_num):
            word_n = most_similars_indexes[n]
            m_n_distance = mass(matrix[word_m], matrix[word_n])

            distance_matrix[m][n] = m_n_distance
            distance_matrix[n][m] = m_n_distance

    return term_distances, most_similars_indexes, distance_matrix

def compute_graph(party, word, n, mass):

    ##gets necessary preprocessed and computed data
    dict, index, matrix = getCoocurrences(data[party])
    term_distances, dist_indexes, dist_matrix = process_single_word(word, n, matrix, index, mass)
    word_index = index.index(word)

    # print([index[word] for word in dist_indexes])

    #computes labelling for nodes
    labeldict = {}
    labeldict[word_index] = word
    for w in dist_indexes:
        labeldict[w] = index[w]

    #computes sizes for nodes
    node_sizes = [dict[word]]
    node_sizes += [dict[index[i]] for i in dist_indexes]
    node_sizes = [i*10 for i in node_sizes]
    # print('sizes:')
    # print(node_sizes)

    #creates a graph and adds nodes for word and n least distant words
    graph = nx.Graph()
    graph.add_node(index.index(word))
    graph.add_node(word_index)
    for i in dist_indexes:
        graph.add_edge(word_index, i, weight = term_distances[i])


    #adds edges between words those distance ist at least 0.8
    matrix_dim = len(dist_indexes)
    for i in range(matrix_dim):
        for j in range(i + 1,matrix_dim):
            if dist_matrix[i][j] >= 0.8:
            # print(1-dist_matrix[i][j])0
                graph.add_edge(dist_indexes[i], dist_indexes[j], weight= dist_matrix[i][j])

    return graph, node_sizes, labeldict


def plot_all(word, n):

    for i in range(len(parteien)):

        #defines 6 subplots
        plt.subplot(2, 3, i+1)
        plt.title(parteien[i])

        #gets graph and its properties
        graph, node_sizes, labeldict = compute_graph(parteien[i], word, n, spatial.distance.cosine)

        edges = graph.edges()
        weights = [((graph[u][v]['weight'] - 0.7) / 0.3) for u, v in edges]

        #draws graph
        pos = nx.spring_layout(graph)
        nx.draw_networkx_nodes(graph, pos, node_size=node_sizes, node_color=farben[i](0.80))
        nx.draw_networkx_labels(graph, pos, labels=labeldict)
        nx.draw_networkx_edges(graph, pos, edge_color=weights, edge_cmap=farben[i])

    #saves the plot to a $word.png file
    plt.savefig('{}.png'.format(word))
    #outputs the plot
    plt.show()



plot_all('Frauen', 10)

# plot('gruene', 'Herren', 10, spatial.distance.cosine, plt.cm.Greens)
