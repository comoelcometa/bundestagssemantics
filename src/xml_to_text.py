import xml.etree.ElementTree as ET
import collections
import json
import nltk
from nltk.tag import StanfordPOSTagger
import os

path_to_model = '/home/comoelcometa/software/stanford/models/german-fast.tagger'
path_to_jar = '/home/comoelcometa/software/stanford/stanford-postagger.jar'
files_mask = '19{}-data.xml'
tagger = StanfordPOSTagger(model_filename='german-hgc.tagger', path_to_jar=path_to_jar)


def xmlToDict(path, data):
    tree = ET.parse(path)
    root = tree.getroot()

    # data = collections.defaultdict(list)
    for point in root[1]:
        for speech in point.findall('rede'):
            saetze = []
            for element in speech:
                if element.attrib and element.attrib['klasse'] == 'redner':
                    try:
                        fraction = element.find('redner').find('name').find('fraktion').text
                    except Exception:
                        fraction = ''
                print(fraction)
                if element.attrib and (element.attrib['klasse'] == 'J' or element.attrib['klasse'] == 'J_1' or element.attrib['klasse'] == 'O'):
                    text = element.text
                    sents = nltk.sent_tokenize(text)
                    for sent in sents:
                        tagged_words = tagger.tag(nltk.word_tokenize(sent))
                        print(tagged_words)

                        saetze.append([word[0] for word in tagged_words if (word[1] == 'NN' or word[1] == 'VVFIN' or word[1] == 'ADJA')])

                    print(saetze)
                    # print(tagged_words)
                        # print('ausgewaehlte: {}'.format(chosen_words))
                        # saetze.append([word for word in nltk.word_tokenize(sent) if word.isalpha()])
                        # saetze.append([word for word in nltk.word_tokenize(sent) if word.isalpha()])

            if fraction and saetze:
                if fraction.find('CDU/CSU') is not -1:
                    # print('ja')
                    data['union'] += saetze
                if fraction.find('GRÜNEN') is not -1:
                    data['gruene'] += saetze
                if fraction.find('AfD') is not -1:
                    data['afd'] += saetze
                if fraction.find('SPD') is not -1:
                    data['spd'] += saetze
                if fraction.find('FDP') is not -1:
                    data['fdp'] += saetze
                if fraction.find('LINKE') is not -1:
                    data['linke'] += saetze

    return data

def preprocessAllProtocols():
    data = collections.defaultdict(list)
    for i in range(108, 117):
        xmlToDict(files_mask.format(i), data)

    # print(data)
    return data

def dataToJSON():
    data = preprocessAllProtocols()
    with open('data.json', 'w') as file:
        json.dump(data, file)

dataToJSON()
